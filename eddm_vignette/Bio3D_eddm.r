#'---
#'title: "Introduction to Ensemble Difference Distance Matrix (eDDM) Analysis"
#'author: "Barry J. Grant, Lars Skjærven, and Xin-Qiu Yao"
#'affiliation: University of California San Diego
#'date: "`r Sys.Date()`"
#'output:
#'  html_document:
#'    toc: true
#'    toc_depth: 3
#'    toc_float: true
#'    collapsed: false
#'    fig_caption: true
#'    fig_height: 5
#'    fig_width: 5
#'    out_width: 50%
#'    df_print: paged    
#'    number_sections: true
#'  pdf_document:
#'    toc: true
#'    toc_depth: 3
#'    fig_caption: true
#'    fig_height: 5
#'    fig_width: 5
#'    highlight: default
#'    keep_tex: true
#'    number_sections: true
#'documentclass: article
#'fontsize: 11pt
#'geometry: tmargin=2.5cm,bmargin=2.5cm,lmargin=2.5cm,rmargin=2.5cm
#'linkcolor: black
#'bibliography: bio3d.bib
#'---

#' \newpage
#' 
#' \setcounter{secnumdepth}{3}

#+ setup, include=FALSE
knitr::opts_chunk$set(dev='png', dev.args=list(type="cairo"), dpi=160)
#knitr::opts_chunk$set(fig.path='figures/', dev='png', dev.args=list(type="cairo"), dpi=160)
#knitr::opts_chunk$sert(tidy=TRUE, tidy.opts=list(width.cutoff=60))
sysopts <- getOption("width")
options(width=80)

#+ preamble, include=FALSE, eval=FALSE
library(rmarkdown)
render("Bio3D_eddm.r", "all", clean=FALSE)

#' # Background {-}
#' The ensemble difference distance matrix (eDDM) analysis is a computational approach to comparing structures of proteins and other biomolecules[@Grant20]. It calculates and compares atomic distance matrices across large sets of homologous atomic structures to help identify the residue wise determinants underlying specific functional processes. The approach supports both experimental and theoretical simulation-generated structures. A typical eDDM workflow includes: 
#' 
#' + **Collecting and preparing the structure set** 
#' + **Grouping structures**
#' + **Calculating difference distance matrices and associated statistics**
#' + **Identifying and visualizing significant distance changes.** 
#' 
#' The approach is implemented in the Bio3D package, **bio3d.eddm**. It can be integrated with other Bio3D methods[@grant06; @skjaerven14] for dissecting sequence-structure-function relationships, and can be used in a highly automated and reproducible manner. **bio3d.eddm** is distributed as an open source R package available from: http://thegrantlab.org/bio3d.
#' 
#' #### Requirement: {-}
#' The latest **bio3d** and **bio3d.eddm** are required. See the [Installing Bio3D Vignette](http://thegrantlab.org/bio3d) for more detail. Once installed, type the following command to start using the package (Note that the dependent bio3d package is loaded automatically):
#+ install1, eval=FALSE, include=FALSE
devtools::install_bitbucket("Grantlab/bio3d/ver_devel/bio3d")
 
#+ install2, eval=FALSE, include=FALSE
devtools::install_bitbucket("Grantlab/bio3d-eddm", dependencies=TRUE)

#+ library, message=FALSE
library(bio3d.eddm)

#' # Collecting and preparing the structure set
#' In this vignette, we will use G protein-coupled receptors (GPCRs) as an example to illustrate the eDDM approach. The embedded code below can be used to reproduce the results described in the associated paper[@Grant20]. Specifically, we will analyze structures of the beta adrenergic receptor ($\beta$AR). 

#'
#' ## Using the pre-assembled structure set
#' Users are recommended to use the dataset shipped with **bio3d.eddm** for just to reproduce the results described in this document. This dataset contains 20 pre-assembled $\beta$AR structures. However, to apply the approach to other systems, users need to prepare the structure set from scratch (see next subsection).
#+ load_data, message=FALSE
attach(gpcr)

#' ## Getting the structure set from scratch
#' In this subsection, the detailed procedure to prepare the pre-assembled "gpcr" dataset will be described.^[Results are subject to the dynamic status of online servers, databases, etc. and may not be exactly same as the reported.] Similar steps are required preceding other Bio3D analysis methods, such as principal component analysis (PCA) and ensemble normal mode analysis (eNMA) (see relevant [Bio3D vignettes](http://thegrantlab.org/bio3d)). 
#' 
#' *For users who simply want to reproduce the analysis reported in this document, this subsection can be skipped.*
#' 
#' Starting with the sequence of the human $\beta$-2 adregenic receptor (UNIPROT: P07550), we search the PDB database to find structures with the same or similar sequence to the query, using the BLAST method implemented in Bio3D. 
#+ prep, warning=FALSE, results="hide", cache=TRUE, eval=FALSE
aa <- get.seq("P07550")
blast <- blast.pdb(aa)  

#'
#' The identified structures are sorted in the descending order of sequence similarity to the query. A similarity threshold (on the E-value) is required to cut the searching result and determine the structure set for subsequent analyses. The Bio3D function `plot.blast()` or simply `plot()` can help find a suitable threshold by examining the distribution of the similarity scores.
#+ hit, eval=FALSE
hits <- plot(blast, cutoff=301)

#+ plot_blast, echo=FALSE, fig.cap="**BLAST report regarding the search for beta adregenic receptor structures.**"
#' ![**BLAST report regarding the search for beta adregenic receptor structures.**](figures/blast.png){width=50%}

#'
#' The structures can be further filtered by structural resolution (for crystallographic and cryo-EM structures). Such information can be automatically obtained using the Bio3D function, `pdb.annotate()`.
#+ str_filter, warning=FALSE, eval=FALSE
annotation <- pdb.annotate(hits)
pdb.id <- with(annotation, subset(hits$pdb.id, resolution<= 3.5))
annotation <- subset(annotation, resolution <= 3.5)

#'
#' Selected structrues are then downloaded through the Bio3D function, `get.pdb()`. The optional **split=TRUE** splits the downloaded files into individual chains to facilitate subsequent analyses.
#+ download, results="hide", warning=FALSE, cache=TRUE, eval=FALSE
files <- get.pdb(pdb.id, path="pdbs", split=TRUE)

#'
#' All selected structures are **aligned**. This is to facilitate structural comparisons between "equivalent" or aligned residues.
#+ align, results="hide", cache=TRUE, eval=FALSE
pdbs <- pdbaln(files)

#'
#' #### Side-note: {-}
#' *A good practice* is to save the generated `pdbs` and `annotation` for future uses. This can be done by following commands: 
#+ save, eval=FALSE
mydata <- list(pdbs=pdbs, annotation=annotation)
save(mydata, file="mydata.RData")

#'
#' To load the saved dataset, type following commands:
#+ demo_load_data, eval=FALSE
load("mydata.RData")
attach(mydata)


#'
#' # Grouping structures  
#' The eDDM analysis compares structural ensembles under distinct ligation, activation, etc. conditions. **At least two groups** of structures are required. The grouping of structures can be either from available structural annotations (e.g., the ligand identity bound with each structure; such information is available in the above prepared `annotation` object) or from a structural clustering analysis. The latter approach is more general and requires minimal prior knowledge about the system. In the following example, we use PCA of the distance matrices (through the bio3d function, `pca.array()`) followed by a conventional hierarchical clustering in the PC1-PC2 subspace to get the intrinsic grouping of the $\beta$AR structures.
#' 
#' First, we need to update the aligned structures to include all heavy atoms. This is done by the bio3d function, `read.all()`. 
#' 
#' If the pre-assembled "gpcr" dataset shipped with the bio3d.eddm package is used through `attach(gpcr)`, raw PDB data must be downloaded first.
#+ build_aa_pdbs, results="hide", warning=FALSE, cache=TRUE
get.pdb(pdbs$id, split=TRUE, path="pdbs")
pdbs.aa <- read.all(pdbs, prefix="pdbs/split_chain/", pdbext=".pdb")

#'
#' Otherwise, if the dataset was rebuilt from scratch, type:
#+ build_aa_pdbs2, results="hide", cache=TRUE, eval=FALSE
pdbs.aa <- read.all(pdbs)

#'
#' In the following, we will calculate distance matrices, perform PCA, and cluster structures.
#+ dm, results="hide", cache=TRUE
# Calculate distance matrices.
# The option `all.atom=TRUE` tells that all heavy-atom coordinates will be used.
dm <- dm(pdbs.aa, all.atom=TRUE)

#+ pca
# Perform PCA of distance matrices.
pc <- pca.array(dm)

#+ screeplot, fig.cap="**Scree plot of PCA.** It indicates that the first principal component (PC1) is dominant over other PCs."
# See Figure 2.
plot.pca.scree(pc)

#+ hc
# Perform structural clustering in the PC1-PC2 subspace.
hc <- hclust(dist(pc$z[, 1:2]))
grps <- cutree(hc, k=3)

#+ conformerplot, fig.cap="**Conformer plot.** Each point represents a structure with point color indicating the membership ID of the clustering. Three major clusters or groups are identified: Group 1 (black) representing inactive GPCR structures, Group 2 (red) representing active GPCR structures, and Group 3 (green)."
# See Figure 3.
plot(pc, pc.axes=c(1,2), col=grps)


#'
#' # Calculating difference distance matrices and associated statistics
#' The `eddm()` function calculates the difference mean distance between groups for each residue pair and statistical significance assessed using a two-sample Wilcoxon test. Long-distance pairs in all structures are omitted. This step is controlled by the `mask` option of the function. By default, the function uses `mask="effdist"`, which calculates “effective” distances where changes for long-range residue pairs are scaled down to zero while changes involving short-range interacting residues are kept intact. This "mask" method is designed to assure that at long distances all residue pairs are indistinguishable, resembling an energetic perspective where the residue-residue interaction becomes zero at long distances. 
#' 
#' In the following example, we use `mask="cmap"`. In this "masking" mode, internally calculated contact maps will be used to exclude residue pairs of (constantly) long distance (i.e., pairs that do not show a stable contact in any structural group). An all heavy atom-based method is used by default to calculate contacts. Specifically, a pair of residues are defined to be in contact if their minimal (heavy or non-hydrogen) atomic distance is less than a threshold (by default, 4 angstrom; popular choices are in the range of 4~5 angstrom). Contact maps for each group of structures are summarized – For each residue pair, if the contact probability across all structures within a group is over a probability threshold, 80%, a value of 1 is set for the pair in this group; otherwise, the value is set to be NA (i.e., treated as "missing data"). Other probability thresholds and distance cutoffs can be used by passing arguments to the `eddm()` function (see `?eddm` for more detail). 
#' 
#' Using `mask="cmap"`, we can also identify "switching" residues, i.e., residues showing rearranged contact networks in different groups (see next section). 
#+ eddm, cache=TRUE, results="hide"
tbl <- eddm(pdbs.aa, grps=grps, dm=dm, mask="cmap")

#'
#' # Identifying and visualizing significant distance changes
#' ## Identifying significant distance changes
#' In this step, **significant** distance changes are identified by using the `subset.eddm()` or simply `subset()` function. A significant change is defined by a *p*-value of the statistical test lower than the threshold **alpha** and the absolute mean distance change is above the threshold **beta**. Below, we use **alpha=0.005** and **beta=1.0 (angstrom)**. Also, only **"switching"** residues are returned, i.e., residues showing rearranged contact networks in different groups. This is done by setting `switch.only=TRUE`. For clarity, only Group 1 (representing "inactive" GPCR structures) and Group 2 ("active" GPCR structures) are compared.
#+ subset
keys <- subset(tbl, alpha=0.005, beta=1.0, switch.only=TRUE)
keys

#'
#' In the above table, various results are reported, including residue IDs, contact and distance changes, and associated statistics. For a more detailed explanation of each column, see `?eddm`.

#'
#' #### Side-note: {-}
#'We recommend that users produce exploratory plots of the **alpha** and **beta** parameters. This is similar to how researchers examine differential expression from RNA-Sequencing experiments – for example, plotting $-log(p-value)$ vs $log_{2}(Fold)$ change. Such plots can help researches examine various cut-off values and ultimately the robustness of their results and conclusions.

#'
#' ## Visualizing results
#' Results can be viewed as a 2D plot showing the distribution and magnitude of significant changes along the protein sequence. Optionally, select pairs can be labeled. Here, we label the two regions mentioned in the associated paper[@Grant20], i.e., the region involving the intra-cellular loop 2 (ICL2) (row #1-3 in `keys`) and the region in the middle of trans-membrane (TM) helices (row #17-19 in `keys`). Other types of plot are available by setting the `type` argument of the function (See `?plot.eddm` for more detail).  
#+ eddmplot1, fig.cap="**The 'tile' plot of identified significant distance changes.** Each tile represents a residue pair with color and size scaled by the associated distance change from Group 1 to Group 2 (unit: angstrom)."
# See Figure 4.
plot(keys, pdbs=pdbs, full=TRUE, resno=NULL, sse=pdbs$sse[1, ], type="tile", 
     labels=TRUE, labels.ind=c(1:3, 17:19))

#'
#' Box-whisker plots of select pairs can also be generated for more specific comparisons. 
#+ eddmplot2, warning=FALSE, message=FALSE, fig.cap="**Box-whisker plots of select residue pairs.**", fig.width=7, fig.height=5 
# See Figure 5.
p <- boxplot(keys, dm, grps, inds = c(1:3,17:19))
p <- p + ggplot2::scale_color_manual(values=c("gray30", "#F8766D"), name="Group")
print(p)

#'
#' Note that in the above, we use the `scale_color_manual()` function from the **ggplot2** package to overide the default coloring scheme, in order to match the coloring in the PCA conformer plot (**Figure 3**). 
#'
#' Finally, 3D views of identified residue pairs mapped onto GPCR structures are generated. This is done by the `pymol.eddm()` (invoked by calling `pymol()`) Bio3D function. Before that, structural fitting is performed to facilitate the visual comparision.^[Structural fitting is not a required step for the eDDM calculation itself.]
#+ core, results="hide", cache=TRUE
core <- core.find(pdbs)

#'
#' Again, the following step is dependent on whether the pre-assembled or reconstructed dataset is used.
#' 
#' If using the pre-assembled "gpcr" dataset:
#+ pdbfit, results="hide", cache=TRUE
xyz <- pdbfit(pdbs, inds=core, outpath="fitlsq", prefix="pdbs/split_chain/", 
              pdbext=".pdb")

#'
#' Otherwise, if using the dataset constructed from scratch:
#+ pdbfit2, results="hide", cache=TRUE, eval=FALSE
xyz <- pdbfit(pdbs, inds=core, outpath="fitlsq")

#' 
#' For convenience, we update the stored coordinates using the "fitted" structures.
pdbs$xyz <- xyz

#'
#' We then view all identified key residue pairs showing significant distance changes using the `pymol()` Bio3D function. Note that the function generates a script file that needs to be opened by the **PyMol** software. In the following example, the key residues are displayed as "sticks", through specifying `as="sticks"` in the function. See `?pymol.eddm` for more types of representations.
#+ pymols, eval=FALSE
# See Figures 6-8.
pymol(keys, pdbs=pdbs, grps=grps, as="sticks")

#'
#' ![**Use of `pymol()` to visualize all identified residue pairs (as sticks).** Two GPCR structures are superimposed (green, inactive from PDB: 2R4R; cyan, active from PDB: 3SN6).](figures/pymol_sw_all.png){width=80%}
#'
#'
#' ![**A close view of the switching region near ICL2.** Two GPCR structures are superimposed (green, inactive from PDB: 2R4R; cyan, active from PDB: 3SN6).](figures/pymol_sw1.png){width=50%}
#'
#'
#' ![**A close view of the switching region in the middle of TM helices.** Two GPCR structures are superimposed (green, inactive from PDB: 2R4R; cyan, active from PDB: 3SN6).](figures/pymol_sw2.png){width=50%}
#'
#'

#+ detach
detach(gpcr)

#'
#' # About this document {-}
#' 
#' This document is generated by the **rmarkdown** R package. To reproduce it, simply type following commands:^[The PyMol images need to be prepared separately and to be inserted manually.]
#+ close, include=TRUE, eval=FALSE
library(rmarkdown)
render("Bio3D_eddm.r", "all")

#'
#' # Information about the current Bio3D session {-}
#+ session
print(sessionInfo(), FALSE)

#'
#' # References {-}
#' 
#+ end, echo=FALSE, include=FALSE
options(width=sysopts)
